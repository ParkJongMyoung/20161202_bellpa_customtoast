package bellpa.example.customtoast;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button button_toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button_toast = (Button)findViewById(R.id.button_toast);

        button_toast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setToast();
            }
        });
    }

    private void setToast(){

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.view_toast, null);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.TOP,0, dpToPixel(getApplicationContext(), 100));
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);

        toast.show();
    }

    /**
     * dp를 단말 해상도에 따른 pixel 로 바꿔준다.
     * @param context
     * @param dp
     * @return
     */
    public int dpToPixel(Context context, int dp){
        int px= 0;
        if(context == null)
            return px;

        WindowManager window = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        window.getDefaultDisplay().getMetrics(metrics);
        float logicalDensity = metrics.density;

        px = (int) Math.ceil(dp * logicalDensity);
        return px;
    }
}
